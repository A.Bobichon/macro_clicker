import tkinter as tk
import pynput
import numpy as np
import time

from pynput import mouse
from pynput.mouse import Button, Controller, Listener


mouseController = Controller()

def on_move(x, y):
    print('Pointer moved to {0}'.format(
        (x, y)))


def on_click(x, y, button, pressed):
    if pressed:
        if button == Button.left:
            macro.append('{0}, {1}'.format(x, y, button))
            print(macro)
        else:
            listener.stop()
            np.savetxt('save.txt', macro, delimiter='  ', header=nom.get(), comments='', fmt='%s')
            btn2 = tk.Button(root, text="do the macro", command=doTheMacro)
            btn2.pack()
            print(macro)


def macroRegister():
    listener.start()



def doTheMacro():

    def mouseClick():
        print(pos[0], pos[1])
        mouseController.position = (pos[0], pos[1])
        mouseController.click(Button.left, 1)

    
    macro = np.loadtxt("save.txt", delimiter=",",skiprows=1)
    print(macro)
    
    for pos in macro:
        time.sleep(3)
        mouseClick()





listener = mouse.Listener(
    on_move=on_move,
    on_click=on_click,
    )
    

macro = []

# APPLICATION
root = tk.Tk()
root.title("MacroFus")
root.geometry("300x200")
nom = tk.StringVar() 
entry = tk.Entry(textvariable=nom)
btn1 = tk.Button(root, text="register", command=macroRegister)
btn1.pack()
entry.pack()

root.mainloop()






